public class MergeArrays {

    public static void mergeArrays(int[] X, int[] Y) {
        
        int i = X.length - Y.length - 1;
        int j = Y.length - 1;

        // Start merging from the end
        while (i >= 0 && j >= 0) {
            if (X[i] > Y[j]) {
                X[i + j + 1] = X[i];
                i--;
            } else {
                X[i + j + 1] = Y[j];
                j--;
            }
        }

        
        while (j >= 0) {
            X[j] = Y[j];
            j--;
        }
    }

    public static void main(String[] args) {
        int[] X = {1, 3, 5, 7, 0, 0, 0}; 
        int[] Y = {2, 4, 6};             
        
        //For User Input

        
        mergeArrays(X, Y);

        // Print the merged array
        for (int num : X) {
            System.out.print(num + " ");
        }
    }
}
